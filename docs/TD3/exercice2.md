---
author: Benoît Domart
title: Exercice 2 - Salut !
tags:
    - 0-simple
---

# Exercice 2 - Salut !

Reprendre l'exercice 2 du TD précédent ([Cf. ici](../../TD2/exercice2){target=_blank}).

Modifier la méthode `doPost` de cette Servlet, pour **qu'elle redirige vers une JSP** affichant le texte suivant :

`Salut <prénom_indiqué>`

où `<prénom_indiqué>` est le prénom indiqué par l'utilisateur dans la page précédente :

<center>
    ![Exemple](./exemple_prenom.gif)
</center>

???+note "Remarque"

    On a fait ce qui était indiqué dans le bloc "Attention, on ne fera en fait pas ça en pratique !" de l'exercice 2 du TD précédent ...

???success "Problème d'encodage"

    Par défaut, tu as certainement un problème avec l'accent de "prénom".

    Pour le corriger, il faut indiquer au navigateur quel ensemble de caractères utiliser. Le plus complet est UTF-8.

    Pour indiquer qu'on utilise cet encodage, il faut ajouter la balise `<meta>` ci-dessous, à l'intérieur de la balise `<head>` :

    ```html
    <meta charset="UTF-8">
    ```