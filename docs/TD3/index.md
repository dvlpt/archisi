# TD3 - Les JSP 📄

L'objectif de ce TD est de se familiariser avec les JSP, et de découvrir une nouvelle notion liée aux Servlet : la session.

Concrétement, nous allons mettre en place une architecture de ce type :
<center>![archi td3](../images/archi-2tiers-4.png)</center>