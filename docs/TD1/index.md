# TD1 - Premier projet Web 🐣

L'objectif de ce TD est de créer un premier projet Web et de bien comprendre sur des exemples ce que sont les requêtes HTTP `GET` et `POST`.

Concrétement, nous allons mettre en place une architecture de ce type :
<center>![archi td1](../images/archi-2tiers-4.png)</center>