---
author: Benoît Domart
title: Exercice 3 - Requêtes HTTP
tags:
  - 0-simple
---

# Exercice 3 - Requêtes HTTP

Maintenant que nous avons créé ce projet Web, nous allons le **compléter**, le **déployer** et le **tester**.

## 1. Récupération d'un contenu Web

Télécharge le ZIP ci-dessous, et dépose son contenu dans le dossier 📂`webapp` du projet.

<center>
    [ContenuWeb.zip](./ContenuWeb.zip){ .md-button download="ContenuWeb.zip" }
</center>

???+success "Arborescence du projet"

    L'arborescence du projet doit donc maintenant être (avec **en gras** ce qui vient d'être ajouté) :

    <pre style="line-height: 1.2;">
    PremierProjet/
    └── src/
        └── main/
            ├── java/
            └── webapp/
                ├── **css/**
                │   └── **style.css**
                ├── **javascript/**
                │   └── **jscript.js**
                ├── META-INF/
                │   └── MANIFEST.MF
                ├── WEB-INF/
                │   └── lib/
                │   └── web.xml
                └── **testHttp.html**
    </pre>

## 2. Création d'un serveur local<a name="creation-serveur"></a>

Pour tester notre application, il faut la déployer sur un serveur (tomcat9), et il faut donc au préalable créer une instance de tomcat9 sur notre poste.

Pour cela :

1. Il faut aller dans la vue `Servers` dans _eclipse_ (si elle n'est pas affichée, il faut aller dans :fontawesome-solid-bars:`Window > Show View > Servers`).
2. Dans la vue `Servers`, faire un clic droit, puis :material-mouse:`New > Server`.
3. Naviguer vers :material-file-tree:`Apache > Tomcat v9.0 Server`.
4. :warning: Il faut **vérifier** qu'il y a bien la bonne _Server runtime environment_ (encore une fois, pour l'instant, il n'y en a qu'une, mais il y en aura bientôt plusieurs !)
5. On peut directement cliquer sur <bouton>Finish</bouton>.
6. :warning: Dans la vue `Servers`, double-cliquer sur le serveur Tomcat, dans la partie `Server Locations`, sélectionner `Use Tomcat Installation`, et enregistrer.

    <center>
      ![Use Tomcat installation](../images/use_tomcat_installation.png)
    </center>
7. Le serveur peut maintenant être démarré :
    <center>
      ![Démarrer un serveur dans Eclipse](../images/demarrer_serveur.png)
    </center>
    <center>(1) pour le démarrer normalement, (2) pour le démarrer en mode debug.</center>
8. On vérifie dans les _logs_ du serveur que tout s'est bien passé.

    Pour cela, on affiche la **console** :
    <center>
      ![Afficher les logs](../images/afficher_console.png)
    </center>

    La dernière ligne doit ressemble à cela :
    ``` title=""
    INFOS: Le démarrage du serveur a pris [179] millisecondes
    ```


## 3. Déploiement de l'application sur ce serveur<a name="deploiement-application"></a>

Pour déployer notre application **_PremierProjet_** sur ce serveur (on aurait pu le faire directement en créant ce serveur), il faut faire un clic droit sur le serveur, puis :material-mouse:`Add and Remove...`, puis ajouter le projet **_PremierProjet_** (le mettre à droite), et enfin cliquer sur <bouton>Finish</bouton>.

**Après avoir redémarré le serveur**, on vérifie dans les _logs_ du serveur que tout s'est bien passé.

???+success "Redémarrage du serveur"

    De manière générale, après chaque modification (ajout, suppression ou modification d'un fichier, quel qu'il soit), on redémarrera le serveur.


## 4. Études des requêtes HTTP

Notre application contient une page, qui est disponible à l'adresse [http://localhost:8080/PremierProjet/testHttp.html](http://localhost:8080/PremierProjet/testHttp.html){target=_blank}

Accéder à cette page HTML depuis Firefox, ouvrir le menu `Réseau` (++ctrl+shift+"E"++), puis rafraîchir la page (++ctrl+shift+"R"++).

1. Il doit y avoir 5 lignes (sinon rafraîchir à nouveau). Chacune correspond à une requête HTTP.

    Indiquer l'origine de chacune de ces requêtes par rapport au fichier HTML.
2. La première colonne donne le code de retour. Il y a une erreur. La corriger.

    ???+success "Les codes retours"

        Pour rappel, voici les 5 familles de codes retours :

        * 1XX : Réponses informatives.
        * 2XX : Succès.
        * 3XX : Redirection.
        * 4XX : Erreur côté client.
        * 5XX : Erreur côté serveur.

3. La page HTML contient un formulaire (balises `<form>...</form>`) dont la méthode est `GET`.

    ???+success "Un formulaire Web"

        Regarde au passage le code HTML de cette page. Elle contient un **formulaire** (définie par les balises `<form>` et `</form>`), qui va permettre à l'utilisateur de saisir des informations, et de les envoyer à un serveur.

        Différents types d'informations peuvent être renseignées dans un formulaire (du texte, des mots de passe, des fichiers, des checkbox, des radio boutons, ...), et à chaque fois, le code ressemble à `<input type="xxx" name="yyy"/>`.

        Côté serveur, la donnée saisie sera récupérée grâce à la valeur de la propriété `name` (qui doit donc être **différente** pour chaque champ).

    Vérifier que lorsqu'on soumet le formulaire (c'est-à-dire lorsqu'on clique sur <bouton>Valider</bouton>), le mot de passe entré apparaît "en clair" dans l'URL.
    
    1. Que faut-il modifier pour que ça ne soit plus le cas ?
    2. Combien l'URL contient-elle de paramètres ?
    3. Pour analyser une requête HTTP, il faut cliquer sur la ligne qui correspond dans la console de Firefox. On obtient alors plusieurs onglets, qui permettent notamment d'obtenir les en-têtes (de la question et de la réponse), les cookies, la requête, et la réponse.

        Avec la modification apportée précédemment, où sont maintenant indiqués les valeurs des différents paramètres (`leNomDUtilisateur`, `leMotDePasse`, ...) ?
        
    4. Quels sont les différents types de données que l'on peut mettre dans une formulaire HTML ?

        Ajoute les dans la page, et observe comment ces paramètres sont envoyés dans la requête HTTP.