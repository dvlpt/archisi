---
author: Benoît Domart
title: Exercice 2 - Création d'un projet Web dynamique
tags:
  - 0-simple
---

# Exercice 2 - Création d'un projet Web dynamique

Eclipse offre la possibilité de créer plusieurs types de projets Java. Pour pouvoir créer et déployer des _Servlets_, il faut créer un **_projet Web dynamique_** (_Dynamic Web Project_ en anglais).

1. On commence donc par créer un projet Web dynamique :

    Dans eclipse, on se place dans la vue `Project Explorer`. Clic droit avec la souris, puis :material-mouse:`New > Project`. Il faut enfin aller dans :material-file-tree:`Web > Dynamic Web Project` comme dans la capture d'écran ci-dessous :
    
    <center>![Nouveau projet Web dynamique](../images/newDynamicWebProject-a.png)</center>

2. Il faut ensuite donner un nom au projet (**_PremierProjet_** ici), indiquer l'environnement d'exécution (pour l'instant, il n'y en a qu'un, c'est tomcat9 - :warning: il y en aura bientôt plusieurs, et il faudra faire bien attention !), puis cliquer sur <bouton>Next ></bouton> comme sur la capture d'écran ci-dessous :
    <center>![Nouveau projet Web dynamique](../images/newDynamicWebProject-b2.png)</center>
3. :warning: :warning: Il faut ensuite cliquer une fois sur <bouton>Next ></bouton> à nouveau, **mais attention à bien cocher la case _Generate web.xml deployment descriptor_ dans le dernier écran**. On peut ensuite cliquer sur `Finish`.
    <center>![Nouveau projet Web dynamique](../images/newDynamicWebProject-c2.png)</center>

    ??? bug "Pas de web.xml ?"

        Si vous avez oublié de cocher cette case, il faudra soit supprimer ce projet et recommencer à l'étape 1, soit ajouter manuellement le fichier 📄`web.xml`, **en respectant la structure attendue**.

???+ note "Racine de l'application Web"

    Par défaut, le _context root_ de l'application est le nom du projet, ici **_PremierProjet_**.<br>
    Cela signifie que pour accéder au contenu de l'application depuis un navigateur Web, une fois qu'elle sera déployée sur un serveur, toutes les URLs seront du type [http://localhost:8080/PremierProjet/...](http://localhost:8080/PremierProjet/...){target=_blank}

    Cela signifie également qu'on peut déployer autant de projet Web que souhaité sur le serveur tomcat, puisqu'ils auront tous un _context root_ différent, et ils auront donc tous une URL pour y accéder différentes.

???+ note "Arborescence d'un projet Web dynamique"

    Par défaut, voici l'arborescence d'un projet Web dynamique (on peut la modifier à la création du projet dans eclipse, mais c'est déconseillé) :

    <pre style="line-height: 1.2;">
    PremierProjet/
    └── src/
        └── main/
            ├── java/
            │   └── ici, il y a les packages (qui sont des dossiers) et les classes Java.
            └── webapp/ (c'est le contenu Web de l'application)
                ├── META-INF/
                │   └── MANIFEST.MF
                ├── WEB-INF/
                │   └── lib/
                │   └── web.xml
                └── ici, il y a toutes les pages HTML, JSP, CSS, JavaScript ...
                    éventuellement dans des sous-dossiers.
    </pre>

    Ainsi, l'URL [http://localhost:8080/PremierProjet/](http://localhost:8080/PremierProjet/){target=_blank} pointe vers le dossier 📂`webapp`.
    
    Si on place par exemple un script 📄`style.css` dans un sous-dossier 📂`css` du dossier 📂`webapp`, on pourra afficher son contenu dans un navigateur en allant à l'URL [http://localhost:8080/PremierProjet/css/style.css](http://localhost:8080/PremierProjet/css/style.css){target=_blank}

    Il y a également un dossier 📂`build`, au même niveau que 📂`src`, qui va contenir toutes les classes Java compilées (et toutes les ressources).
