# TD9 - Les Web Services

L'objectif de ce TD est de découvrir les Web Services, et leur utilisation avec JAX-WS, l'API JEE gérant les WS :

- Consommation d'un Web Service dans l'exercice 1.
- Implémentation d'un Web Service dans l'exercice 2.

Dans chacun des exercices de ce TD, afin de gagner du temps et se concentrer sur ce qui est nouveau, on se contentera de créer un seul projet web dynamique (on ne créera pas de projet EJB, ni de projet EAR).