---
author: Benoît Domart
title: Exercice 2 - Consommer un WS
---

# Exercice 2 - Consommer un WS avec JAX-WS

Commençons par créer une projet Web Java, que nous appelerons ***TD9***.

L'idée est ici d'intégrer le résultat de l'appel d'un WS dans notre application Web écrite en JEE.

On souhaite créer une application Web offrant un formulaire, dans lequel on indique dans un formulaire Web le code d'un pays, et qui renvoie la capitale du pays correspondant.
<center>
    ![Choix runtime](../gif/ws_country.gif)
</center>

Pour cela, nous allons avoir besoin de créer les couches suivantes :

1. Couche présentation (vue) : Une JSP affichant un formulaire permettant d'indiquer le code du pays et affichant sa capitale lorsque le pays existe.
2. Couche présentation (contrôleur) : Une servlet permettant
    - L'affichage de la JSP (GET).
    - L'exécution du WS et l'affichage de la JSP (POST).
3. Couche métier : appel de du WS. Ici, notre WS correspond plus à un service qu'à un dao, on l'appelle donc depuis la couche métier, et il n'y a pas de couche dao dans notre appli.
    On aurait pu faire le choix de rajouter la couche dao, et d'appeler le WS depuis cette dernière.

Il va donc falloir créer tout le code Java permettant l'appel de ce WS et la récupération du résultat. Heureusement, tout cela est automatique ! C'est le principe du *Contract First*. Tout le code va être automatiquement généré à partir du WSDL.

Pour cela, il faut utiliser l'utilitaire `wsimport` fourni avec la JDK (jusqu'à la version 1.8 du moins).

???+abstract "Générer un client SOAP Java"

	L'exécutable `wsimport`, fourni de base avec la JDK (si la version est inférieure ou égale à 1.8), permet de générer le code Java pour créer un client SOAP.
	
	Deux options sont importantes :
	
	1. `-s` : permet d'indiquer où les sources vont être générées. Le plus simple est de se placer dans le dossier "source" du projet concerné, et d'indiquer `-s .` (ce qui signifie "générer les sources ici").
	2. `-p` : (optionnel) permet d'indiquer le package dans lequel les classes générées vont être placées.
	
	Par exemple, après s'être placé dans le dossier 📂`src/main/java` du projet Web, on pourra écrire
	```bash
	wsimport -s . -p fr.univtours.polytech.td9.business.country http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL
	```
	
	- Comme indiqué plus tôt, le client SOAP va permettre d'implémenter un service, on place donc ces sources dans le package `business`.
	
	- Parfois, le client SOAP sera la source de données. Dans ce cas là, on pourra placer les sources dans le package `dao`.

    - De nombreuses classes peuvent être générées (c'est le cas ici - il y en a 58 normalement), afin de ne pas se mélanger avec le reste de l'application, on les places dans un sous package `country`.

???+abstract "Appeler le WS depuis la couche métier"

    Une fois les classes Java générées, il faut identifier celles qui vont nous permettre d'appeler le WS depuis notre code Java.

    Il y a normalement une interface et une classe qui ont un nom proche de celui du WS. Ici, l'interface est `CountryInfoServiceSoapType` et la classe est `CountryInfoService`.

    Pour appeler le WS, depuis notre couche *business* donc, voici le code qu'il faut écrire :

    ```java title=""
    CountryInfoServiceSoapType ws = new CountryInfoService().getCountryInfoServiceSoap();
    ```

???+abstract "Rappels - Architecture de l'application"

    Pour rappel,

    - La couche métier est composée d'une interface, et d'une classe l'implémentant avec l'annotation `@Stateless`.
    - Dans la servlet, il y a une dépendance vers l'interface métier, avec l'annotation `@EJB`.