# TP noté - Location pour les vacances


## 1 - Présentation du TP

Le but de ce TP est de créer une mini application Web permettant de réserver une location pendant une ou plusieurs nuits.<br>
Pour cela, vous travaillerez en groupe de deux ou trois.

### 1.1 - Ce qui est fait

Un squelette d'application est fourni, qui permet l'affichage des locations disponibles (pour simplifier, on suppose que toutes les locations sont disponibles). L'utilisateur peut également cliquer sur la location qu'il souhaite réserver.

Chaque location a un prix donné, qui est indiqué.

Pour que ce squelette soit fonctionnel, il faut une base de données MySQL, contenant un schéma nommé **location**.


???+ note "Création de la _datasource_"

    Sur le serveur WildFly, il faut créer une nouvelle _datasource_ vers cette base de données.
    
    Pour cela, vous pouvez :
    
    - soit la créer depuis la console d'administration du serveur, comme nous l'avons fait dans la deuxième partie du TD3,
    - soit effectuer un copier/coller du code que cette action a généré dans le fichier 📄`standalone.xml`, accessible depuis la vue serveur (dans :material-file-tree:`WildFly > Filesets > Configuration File`). Il y a alors 3 choses à changer :
    
        1. Le `jndi-name` : indiquer `java:/LocationDB`. C'est ce nom qu'il faut indiquer dans le fichier 📄`persistence.xml`
        2. Le `pool-name` : indiquer `LocationDB`
        3. Le `connection-url` : indiquer `location` comme nom du schéma à la fin de l'URL.


### 1.2 - Ce qu'il faut faire


Pour l'instant, rien n'est affiché lorsque l'utilisateur clique sur une location. Il faut que l'utilisateur soit redirigé vers la page de détail de cette location, dans laquelle :

- Il peut saisir la date de début et la date de fin du séjour.
- Il peut indiquer s'il souhaite prendre le forfait ménage ou pas.
- Il peut indiquer s'il souhaite prendre une assurance annulation ou pas.


L'utilisateur clique ensuite sur le bouton <bouton>Réserver</bouton>, qui le redirige vers une autre page, qui contient le détail de ce qu'il doit payer :

- Le prix du logement (Cf. RG1).
- Le prix du ménage (Cf. RG2).
- Le prix de l'assurance annulation (Cf. RG3).
- Les éventuelles réductions (Cf. RG4, RG5, RG6 et RG7).
- Le prix total (Cf. RG8).

Cette page contient un bouton <bouton>Valider la réservation</bouton> qui effectue l'enregistrement de la réservation dans la base de données.

???+info "Détail sur l'enregistrement de la réservation"

    Il va falloir créer une nouvelle table dans la base de données, permettant de recenser toutes les réservations qui ont été effectuées.

    Il s'agit d'un travail important, qui peut être fait en dernier, lorsque tout le reste a été traité (même remarque pour la mise en place de la RG7 - cf. ci-dessous).

    Dans cette table, il faudra stocker l'identifiant de l'utilisateur ayant effectué la réservation, la data à laquelle cette réservation a été effectuée et son prix total.

    Pour simplifier, on supposera qu'un seul utilisateur accède à cette application, dont l'identifiant est 42.

???+ success "Détails des règles de gestion"

    Voici la liste des règles de gestions à mettre en place dans l'application.

    - RG1 : nombre de nuits multiplié par le prix du logement.
    - RG2 : 20 euros si le forfait ménage a été sélectionné, 0 euro sinon.
    - RG3 : 5% du prix du logement (c'est-à-dire du résultat de la RG1) si l'assurance a été sélectionnée, 0 euro sinon.
    - RG4 : si la durée du séjour est supérieure ou égale à 7 nuits, une nuit est gratuite (uniquement une nuit, cela n'impacte pas le forfait ménage, ou l'assurance annulation, ...).
    - RG5 : si le prix total (RG1 + RG2 + RG3 - RG4) est supérieur à 500 euro, réduction de 10%, sinon pas de réduction.
    - RG6 : si la date de début de la réservation est dans plus de 30 jours, réduction de 7% (sur le résultat de RG5), sinon pas de réduction.
    - RG7 : toutes les 3 réservations effectuées, le client a une remise de 12% de la somme des prix des deux réservations précédentes (cette remise ne peut pas être plus importante que le prix correspondant à la RG6).
    - RG8 : RG6 moins l'éventuelle réduction de la RG7.


???+ info "Détail concernant la RG7"

    La RG7 est la règle de gestion qui nécessite le plus de travail. Elle va de pair avec la création de la table des réservations, et peut être mise en place en dernier, lorsque tout le reste est effectué.
    
    Exemple d'application de cette RG :

    - Le client 42 effectue sa 4ème réservation (coût total 100€) : il n'y a pas de réduction.
    - Le client 42 effectue sa 5ème réservation (coût total 200€) : il n'y a pas de réduction.
    - Le client 42 effectue sa 6ème réservation : il a une réducation de 12% de (100+200)€ soit 36€. Si le coût de sa 6ème réservation est inférieur ou égal à 36€, il ne paie rien (mais il ne gagne rien non plus), sinon, une réduction de 36€ est effectué sur le coût total (RG6) de sa 6ème réservation.


<!--%\textbf{Bonus} RG4 : Si la date de début est le lendemain, il est impossible de sélectionner l'assurance annulation.-->

??? example "Version de test"

    Si le serveur est démarré chez moi, vous pouvez voir [ici](http://176.173.252.138:8081/LocationCorrWeb/) à quoi l'application doit ressembler.

    Attention, il peut y avoir quelques bugs ...
    

## 2 - Remarques & conseils


Voici quelques conseils pour vous aider.

1. Commencez par faire un schéma des différentes pages de l'application, avec les actions possibles de l'utilisateur, et les servlets correspondantes.
2. Chaque clic de l'utilisateur sur un bouton doit exécuter une servlet qui redirige l'utilisateur vers la JSP adaptée.
3. Pour chaque clic, il faut se demander s'il s'agit d'une requête HTTP GET ou HTTP POST.
4. Faites ensuite une application fonctionnelle la plus simple possible (c'est-à-dire uniquement un squelette) :

    - Le clic sur une location amène sur la page de réservation, qui contient uniquement le bouton "Réserver".
    - Le clic sur le bouton "Réserver" amène sur une troisième page très simple, qui affiche par exemple le prix d'une nuit de la location sélectionnée.<br><br>
    Vous ajouterez ensuite au fur et à mesure les fonctionnalités demandées.<br>
    Cela vous simplifiera de plus beaucoup la tâche pour travailler à plusieurs !

5. Il n'est pas demandé :

    - de contrôler la validité des données saisies par l'utilisateur dans la page de réservation d'une location (l'utilisateur peut par exemple sélectionner une date de fin antérieure à la date de début).
    - de formater les coûts intermédiaires et le coût total : ce n'est pas grave si un des coûts est 253.6666666667 par exemple.<br><br>
    Si vous avez le temps, vous pouvez bien sûr traiter ces points.

6. Pour rappel, un _DAO_ n'effectue l'accès qu'à une seule table en BDD. S'il y a plusieurs tables, il doit y avoir plusieurs _DAOs_ ...
7. En revanche, une classe de la couche métier peut effectuer des appels à plusieurs _DAOs_ (et donc appeler des requêtes sur plusieurs tables).

??? tip "Conseils supplémentaires"

    1. Une erreur classique est de ne pas mettre les _JSPs_ au bon endroit. Il faut bien les mettre dans le dossier 📂`webapp`.
    2. Votre application va contenir deux sortes de _beans_ :
        - Ceux qui représentent une table de la BDD. Il faut alors créer une "Entité JPA" (annotation `@Entity`), qui est recensée dans le 📄`persistence.xml`.
        - Ceux qui permettent uniquement de stocker des informations, par exemple tout ce qu'un utilisateur peut renseigner dans un formulaire. Il s'agit alors d'un _bean_ "classique", c'est-à-dire une classe qui implémente `Serializable`, avec un constructeur public sans argument, et des accesseurs pour accéder aux propriétés.
    3. Ne laissez pas de code inutile. Par exemple, nous n'utilisons pas les constructeurs des _Servlets_. Pensez donc à les supprimer.
    4. Attention aux imports que vous faites. De nombreuses classes ont le même nom, il faut donc bien vérifier que vous importez celle du bon package.
    5. Lorsque vous effectuez une modification de votre code, il n'est pas nécessaire de redémarrer le serveur WildFly. Vous pouvez effectuer un clic droit sur le projet déployé sur le serveur (dans la vue `Serveur` dans eclipse) et cliquer sur "_Full Publish_". Cela permet de gagner un peu de temps.


## 3 - Récupération du squelette existant

Vous pouvez récupérer le zip présent sur Celene.

Il contient 4 projets Java (3 projets JEE et un projet Java) :

- Un projet EJB : **_LocationAppEJB_**
- Un projet Web : **_LocationAppWeb_**
- Un projet EAR : **_LocationApp_**
- Un projet Java "classique" : **_LocationAppClient_**

Ces projets peuvent directement être importés dans eclipse. Il suffit, dans la vue _explorer_,

- de faire un clic droit, puis :material-mouse:`Import > Import...`,
- puis :material-file-tree:`General > Existing Projects into Workspace`,
- puis <bouton><u>N</u>ext ></bouton>,
- d'indiquer le dossier où sont présents ces 4 projets, de les sectionner,
- et enfin de cliquer sur <bouton><u>F</u>inish</bouton>.

???+ note "Sélection de la _runtime environment_"

    Une _runtime_ WildFly est associée au trois projets JEE. Pour qu'ils soient fonctionnels, il faut vérifier que cette association se fait bien. Si vous rencontrez une erreur (par exemple parce que votre runtime ne s'appelle pas exactement "WildFly 26.0.1 Runtime"), il suffit de faire un clic droit sur le projet JEE concerné,
    
    - puis :material-mouse:`Properties`,
    - puis :material-file-tree:`Project Facets`,
    - de sélectionner l'onglet `Runtimes`,
    - et enfin de sélectionner la bonne _runtime_.

???+ note "Ajout du JAR client pour le projet client"

    Pour que le projet client soit fonctionnel, comme dans le TD précédent, il faut ajouter le JAR WildFly le permettant.

    Pour rappel, il s'agit de 📄`<WILDFLY_HOME>/bin/client/jboss-client.jar`.

    Vous pouvez également créer une variable nommée `JBOSS_CLIENT_JAR` pointant vers ce jar.

???+ success "Renommages des projets"

    Avant toute chose, commencez par renommer vos projets JEE (tous sauf le projet client) en indiquant votre numéro de groupe. Par exemple, si vous faites parties du groupe 9, vous devez avoir **_LocationApp09EJB_**, **_LocationApp09Web_** et **_LocationApp09_**.

    Pour cela, il faut :

    - faire un clic droit sur le projet,
    - puis :material-mouse:`Rename...` (pour le projet _EAR_) ou :material-mouse:`Refactor > Rename...` (pour les projets _Web_ et _EJB_).

    Les liens vers les projets _EJB_, depuis le _Web_ et le client sont alors rompus. Il faut les mettre à jour manuellement sur ces 2 projets, via
    
    - :material-mouse:`Properties`,
    - :material-file-tree:`Java Build Path`,
    - onglet `Projects`,
    - supprimer le projet _EJB_ manquant et le remplacer par le nouveau.

    Enfin, dans la classe `LocationClient` du projet client, il faut mettre à jour le nom JNDI de l'EJB, en modifiant les valeurs des variables `appName` et `projectName` aux lignes 71 et 72.

???+ success "Démarrage du projet"

    1. Démarrer le serveur WildFly, déployer le projet **_LocationApp_** et vérifier que le table `LocationBean` a bien été créée.
    2. Exécuter la classe `LocationClient` du projet du même nom, et vérifier que les 4 enregistrements ont bien été insérés en base de données.
    3. Vous pouvez ajouter d'autres locations. Si vous souhaitez mettre une image, il faut l'ajouter dans le dossier `images` du projet **_LocationAppClient_**.

    L'application est accessible via [http://localhost:8080/LocationAppWeb/](http://localhost:8080/LocationAppXXWeb/) (en remplaçant XX par votre numéro de groupe, et en adaptant éventuellement le port).

???+ tip "Drop and Create"

    Dès que vous effectuez des modifications de votre modèle, par exemple en ajoutant une table, ou en en modifiant une existante (ajout, suppression ou modification d'une colonne par exemple), pensez à republier votre application en mode `drop and create` (dans le 📄`persistence.xml`). Cela permet de mettre à jour la base de données.

    Une fois la base mise à jour, vous pouvez republier l'application, cette fois en mode par défaut (c'est à dire en indiquant `default` ou `none`).

## 4 - Rendu du projet

Vous devrez déposer un ZIP sur Celene contenant les 3 projets **_LocationAppEJB_**, **_LocationAppWeb_** et **_LocationApp_**, et éventuellement une documentation de votre projet.



## 5 - Barème

Ce barème est donné à titre indicatif et pourra évoluer.

| Item                                                                                                                       | Point(s) |
| -------------------------------------------------------------------------------------------------------------------------- | -------- |
| L'application est fonctionnelle                                                                                            | 2        |
| L'application respecte la séparation en couche (modèle / vue / contrôleur / couche métier / couche DAO)                    | 6        |
| Dans la page de réservation d'une location, les fonctionnalités demandées sont présentes et fonctionnelles :               | 2        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Sélection d'une date de début et d'une date de fin                 |          |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Sélection du forfait ménage                                        |          |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Sélection de l'assurance annulation                                |          |
| Les 8 règles de gestion métier sont correctement implémentées                                                              | 5        |
| Qualité du code                                                                                                            | 5        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Il n'y a pas de "warning" qui pourraient être facilement supprimés |          |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Le code Java est correctement formaté                              |          |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Il n'y a pas d'import Java inutile                                 |          |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Les variables utilisées ont des noms explicites                    |          |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Là où il le faut, des interfaces sont utilisées                    |          |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Là où il le faut, il y a des commentaires et de la JavaDoc         |          |
| Documentation (optionnel)                                                                                                  | 1        |


<!--
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Mise en place de la RG1                              | 1        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Mise en place de la RG2                              | 1        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Mise en place de la RG3                              | 1        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Mise en place de la RG4                              | 1        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Mise en place de la RG5                              | 1        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Mise en place de la RG6                              | 1        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Mise en place de la RG7                              | 1        |
| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> Mise en place de la RG8                              | 1        |
-->
<!--
        %\hline
        %\textit{Bonus :} Utilisation de Git &                                                                 | 1        |
        %\hline
        %Mini-doc du projet &        |
        %> Découpage des tâches dans le binôme & 0,5        |
        %> Description des fonctionnalités développées & 0,5        |
-->
