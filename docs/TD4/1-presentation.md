---
author: Benoît Domart
title: Présentation du projet
---

# Présentation du projet

L'idée est de créer un site web dynamique, en fait une boutique en ligne, où l'on peut sélectionner différents objets (dans cette première version, uniquement des stylos, des feutres et des gommes), en quantité souhaitée. Notre application indique ensuite le coût du panier, les éventuels frais de livraison, et le coût total de la commande. Trois règles de gestions (RG) doivent pour cela être mises en place :

* RG1 : Calcul du prix du panier, sachant que :
    * un styo coûte 1,20€,
    * un feutre coûte 1,80€,
    * une gomme coûte 2€.
* RG2 : Les frais de livraison sont de 5€ si la commande est de moins de 15€, et sont offerts sinon.
* RG3 : Le prix total de la commande est la somme du prix du panier et des évenutels frais de livraison.

Voici la page (il n'y en a qu'une) de notre site :
<center>
    ![Page d'accueil](../images/BoutiqueWebV1_a.png)
</center>

Le client peut sélectionner le nombre de chaque objet désiré. Lorsqu'il clique sur <bouton>Valider</bouton>, la partie `Récapitulatif du panier` se met à jour, en appliquant les 3 RG définies ci-dessus.

Voici deux exemples d'applications des RG en fonction des sélections effectuées :
<center>
    ![Page de l'application](../images/BoutiqueWebV1_b80.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    ![Page de l'application](../images/BoutiqueWebV1_c2-80.png)
</center>
