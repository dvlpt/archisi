---
author: Benoît Domart
title: Exercice 2 - Tests V1.0
---

# Exercice 2 - Déploiement de l'application et tests

## 1. Création d'un serveur local

Pour tester notre application, il faut la déployer sur un serveur (tomcat9), et il faut donc au préalable créer une instance de tomcat9 sur notre poste. On utilise celle créée [précédemment](../../TD1/exercice3/#creation-serveur){target=_blank}.

## 2. Déploiement de l'application sur ce serveur

On déploie ensuite l'application **_Boutique_** sur le serveur, comme [vu précédemment](../../TD1/exercice3/#deploiement-application){target=_blank}.

**Après avoir redémarré le serveur**, notre application est disponible à l'adresse [http://localhost:8080/Boutique/controller](http://localhost:8080/Boutique/controller){target=_blank}.

???+success

    Il est possible de déployer plusieurs applications en même temps, sur le même serveur :

    <center>
        ![Page d'accueil](../images/applications_deployees.png)
    </center>


## 3. Test de l'application

Lorsqu'on va ici : [http://localhost:8080/Boutique/controller](http://localhost:8080/Boutique/controller) (méthode _GET_), la page s'affiche correctement. Mais lorsqu'on clique sur <bouton>Valider</bouton> (méthode _POST_), une erreur apparaît :
<center>
    ![Erreur méthode POST](../images/boutique_v1.0_erreur.png)
</center>

C'est normal, dans notre _servlet_, nous n'avons implémenté que la méthode `doGet`, donc les requêtes _GET_ (premier affichage de la page) sont gérées. Mais nous n'avons pas implémenté la méthode `doPost`, ce que nous allons faire maintenant ... pour gérer la soumission du formulaire par l'utlisateur