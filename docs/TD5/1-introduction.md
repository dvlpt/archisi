---
author: Benoît Domart
title: Introduction
---

# Introduction

**Prérequis** : Savoir installer une base de données MySQL sur son poste de travail, et y accéder (par exemple avec [MySQL Workbench](https://www.mysql.com/products/workbench/){target=_blank}, avec [Wamp](https://www.wampserver.com/){target=_blank}, ou encore avec [UwAmp](https://www.uwamp.com/fr/){target=_blank} - version portable de Wamp).

## 1. Présentation

Dans les TDs précédents, nous avons mis en place une architecure 2-tiers de ce type :
<center>![archi td2](../images/archi-2tiers-4.png)</center>

Nous allons maintenant ajouter de la persistance, c'est-à-dire ici une connexion à la base de données. L'objectif est donc de mettre en place l'architecture 3-tiers suivante :

<center>![archi td5](../images/archi-3tiers.png)</center>

Dans les TD2 et TD3, nous avons vu comment implémenter les sous-couches "A" (affichage des données), "G" (gestion des actions de l'utilisateur).<br>
La sous-couche "S" (stockage des données) est implémentée via _MySQL_.<br>
Il nous reste donc à implémenter les sous-couches "T" (traitements métiers, nous l'avons déjà commencé dans le TD4 avec l'interface `StoreBusiness` et la classe `StoreBusinessImpl` qui l'implémente) et "A" (accès au données persistantes).

Le but est de créer une application permettant de saisir les notes d'étudiants à une évaluation, d'afficher la liste des notes saisies ainsi que la moyenne.<br>
Comme lors de toute création de programme informatique, avant de le développer, nous allons commencer par le concevoir.

## 2. Conception de l'application

Notre application va contenir trois pages :

1. La page d'accueil, affichant la liste des notes déjà saisies (c'est-à-dire le n-uplet (matricule, nom, prénom, note)), sous forme de tableau. Il sera possible de :

    1. Cliquer sur le bouton "Ajouter une note".
    2. Cliquer sur une ligne du tableau pour modifier un enregistrement.

2. Une page contenant un formulaire permettant d'ajouter une nouvelle note
3. Une page contenant un formulaire permettant d'en modifier une (c'est-à-dire de modifier le nom, le prénom, ou la note, mais pas le matricule, qui est l'identifiant).

Les 2ème et 3ème pages pourraient (devraient) être la même page _JSP_, mais il est plus simple d'en faire 2 et de ne pas avoir à gérer les deux cas dans la même.

???+ warning "Remarque"

	La partie "modification d'une note" n'est a effectuer que lorsque le TD est terminé, pour ceux qui sont en avance. La première version du projet ne doit contenir que l'affichage de la liste des notes saisies, et la possibilité d'en saisir une nouvelle.

Le schéma suivant indique le fonctionnement de notre application :

<center>![fonctionnement application gestion notes](../images/fonctionnement-gestion-notes.png)</center>

Les flèches correspondent à une action de l'utilisateur, c'est-à-dire à une requête HTTP, et doivent donc être gérées via des _Servlets_.

