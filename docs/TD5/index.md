# TD5 - Connexion à une BDD via JDBC 🗃️

L'objectif de ce TD est de créer une application Web qui se connecte à une base de données (MySQL dans notre cas), en utilisant l'API JDBC (Java DataBase Connectivity).

Nous allons donc écrire nous mêmes les requêtes SQL, dans le code Java, et effectuer _manuellement_ le _mapping_ (c'est-à-dire le lien) entre les objets Java (les _beans_) et les tables de la base de données.

Le _mapping_ est l'opération qui permet de définir quelle propriété de quel _bean_ correspond à quel champ de quelle table.

Concrétement, nous allons mettre en place une architecture de ce type :
<center>![archi td5](../images/archi-3tiers.png)</center>