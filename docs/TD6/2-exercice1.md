---
author: Benoît Domart
title: Exercice 1 - Installation de l'environnement de développement
---

# Exercice 1 - Installation de l'environnement de développement

## 1. Création du serveur WildFly

Il faut commencer par installer le serveur WildFly. Pour cela, il faut télécharger la dernière version ici : [https://www.wildfly.org/](https://www.wildfly.org/){target=_blank}. Il suffit de cliquer sur **_DOWNLOAD THE ZIP_**, puis de le dézipper dans le dossier 📂`jee`. Dans la suite, on appelera 📂`<WILDFLY_HOME>` le dossier où WildFly a été dézippé (c'est-à-dire 📂`jee/wildfly-x.y.z.Final` normalement).

???note "Nouvelle version de JEE"

    Pour chaque version, WildFly propose deux environnements :

    1. "Jakarta EE 8 Full & Web Distribution", qui implémente Java EE 8 (packages en `javax.*`).
    2. "WildFly Preview EE 9.1 Distribution", qui implémente Jakarta EE 9 (packages en `jakarta.*`).

    On choisit ici d'utiliser le premier, qui implémenente JEE8 (c'est la version téléchargée si on clique directement sur **_DOWNLOAD THE ZIP_**).

    Nous avions déjà parlé des ces deux versions en parallèles [ici, dans le bloc "Nouvelle version de JEE"](../../TD4/3-exercice1#java_vs_jakarta){target=_blank}.

Il faut maintenant créer l'**environnement d'éxécution** (_**runtime environment**_ en anglais) correspondant :

1. Aller dans le menu :fontawesome-solid-bars:`Window > Preferences`, puis dans :material-file-tree:`Server > Runtime Environments`. Sélectionner **JBoss ...** :
    <center>
        ![Installation Wildlfy 1/6](../images/install_wildfly_1.png)
    </center>
2. Accepter les termes de la licence :
    <center>
        ![Installation Wildlfy 2/6](../images/install_wildfly_2.png)
    </center>
3. Cliquer sur <bouton>Apply and Close</bouton>.
4. Sélectionner tout, puis accepter :
    <center>
        ![Installation Wildlfy 3/6](../images/install_wildfly_3.png)
    </center>
5. Cliquer sur <bouton>Apply and Close</bouton> et attendre la fin de l'installation qui peut prendre un peu de temps 💤💤💤
6. 💤💤💤
7. Redémarrer eclipse lorsqu'il le demande. Vous pouvez accepter ou refuser l'envoi de statistiques à JBoss.
8. Il est maintenant possible d'ajouter une _runtime_ Jboss. On retourne dans :fontawesome-solid-bars:`Window > Preferences` puis :material-file-tree:`Server > Runtime Environments`.
9. Cliquer sur <bouton>Add...</bouton>, puis dans l'arborescence, aller dans :material-file-tree:`JBoss Community > WildFly 24+ Runtime`.
10. Cocher "Create a new local server", puis cliquer sur <bouton><u>N</u>ext ></bouton> (l'instance du serveur va ainsi directement être disponible dans la vue _Servers_ d'eclipse) :
    <center>
        ![Installation Wildlfy 4/6](../images/install_wildfly_4.png)
    </center>
11. Dans _Home Directory_, indiquer 📂`<WILDFLY_HOME>` (le dossier ou le ZIP a été dézippé), sélectionner la jdk installée, puis cliquer sur <bouton><u>N</u>ext ></bouton> et enfin sur <bouton><u>F</u>inish</bouton> (sans rien modifier dans la dernière page) :
    <center>
        ![Installation Wildlfy 5/6](../images/install_wildfly_5.png)
    </center>

??? note "Create a new local server - oubli"

    Si on a oublié de cocher la case "Create a new local server", il suffit d'aller dans la vue _server_ dans eclipse, et de le créer manuellement, en indiquant la _runtime_ qu'on vient de créer.

???+ note "📄standalone.xml"

    Le serveur apparaît maintenant dans la vue _server_. En ouvrant l'arborescence, dans :material-file-tree:`Filesets > Configuration File`, on constate la présence du fichier 📄`standalone.xml`, qui permet de configurer le serveur :
    <center>
        ![Installation JBOSS 6/6](../images/install_jboss3.png)
    </center>
    Par la suite, on aura besoin de modifier ce fichier, il suffira de double cliquer ici pour l'ouvrir dans eclipse.

## 2. Accès à la console d'administration du serveur

On peut maintenant démarrer le serveur depuis eclipse. Attention à bien vérifier que le serveur tomcat est arrêté. Les deux étant accessibles via le même port par défaut (8080), il y aura un problème si le tomcat est démarré.

Une fois le serveur démarré, on accède à sa page d'accueil via [http://localhost:8080](http://localhost:8080){target=_blank}. Lorsqu'on clique sur le lien ***Administration Console***, un nom d'utilisateur et un mot de passe sont demandés. Il n'y en a pas par défaut, il va falloir en créer un.

Pour cela, il faut exécuter le script 📄`<WILDFLY_HOME>/bin/add-user.bat` sous windows, ou 📄`<WILDFLY_HOME>/bin/add-user.sh` sous linux.

Nous allons créer un *Management User* (a), `admin/admin`. Pour des raisons de sécurité, cet utilisateur/mot de passe est **fortement** déconseillé, mais ici, il s'agit uniquement de développement, donc nous allons tout de même le faire. Ce script nous demandera de ce fait de confirmer notre choix. Il faut donc entrer, dans l'ordre, `a / admin / a / admin / yes / admin / <vide> / yes`, comme ci-dessous :
<center>
    ![Add user](../images/WildFly-adduser.png)
</center>

On peut maintenant accéder à la console d'administration (même sans redémarrer le serveur).
