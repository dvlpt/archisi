---
author: Benoît Domart
title: Exercice 1 - Une première Servlet
tags:
  - 0-simple
---

# Exercice 1 - Une première _Servlet_

Maintenant que notre premier projet Web dynamique est créé, nous allons pouvoir créer des _Servlets_.

1. Pour en créer une, il faut aller dans la vue `Project Explorer`, sur le projet **_PremierProjet_**, faire un clic droit, puis :material-mouse:`New > Other`. Dans l'arborescence, naviguer ensuite vers :material-file-tree:`Java > Class`.
2. Cliquer sur <bouton><u>N</u>ext ></bouton>.
3. Indiquer le package : `fr.univtours.polytech.premierprojet.servlet`.

    ???+ warning

        On ne laisse jamais le package par défaut lorsqu'on développe une application avec JEE.

4. Indiquer le nom de classe : `MaPremiereServlet`.

    ???+ success "Remarque"

        Il est pratique d'indiquer ce qu'est la classe dans son nom, ici, une _Servlet_.

4. Indiquer que la classe hérite de `HttpServlet` de l'API `servlet` (en cliquant sur <bouton>Brow<u>s</u>e</bouton> en face de _Superclass_), puis clique sur <bouton><u>F</u>inish</bouton>.

    ???+ warning

        Attention, si Eclipse ne trouve pas la classe (abstraite) `HttpServlet`, c'est que tu n'as pas bien configuré le _runtime_.

        Il faut alors :

        1. Vérifier que le _runtime_ "Apache Tomcat v9.0" a bien été créé (et le créer si ce n'est pas le cas).
        2. Faire un clic droit sur le projet, puis :material-mouse:`Properties`, et aller dans le menu `Project Facets`. Il y a alors un onglet `Runtime` où il est possible de cocher le bon _runtime_, si cela n'a pas été fait à la création du projet.

        :warning: Dans tous les cas, ici, il ne faut pas ajouter "manuellement" les JARs. Tout ceux nécessaires sont dans la _runtime_ Tomcat.

5. Recopier le contenu suivant :

    ``` java title="☕ Code Java - Servlet MaPremiereServlet" linenums="1"
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter(); //(1)!
        out.println("<html><head></head><body>Hello World !</body></html>"); //(2)!
    }
    ```

    1. Cet objet va nous permettre d'écrire dans la réponse HTTP.
    2. On écrit directement le code HTML dans la réponse, qui va s'afficher dans le navigateur.

    ???+note "Remarque"

        En programmation, la tradition veut que le premier programme rédigé dans un langage donné soit l'affichage d'[_Hello World !_](https://fr.wikipedia.org/wiki/Hello_world){target=_blank}.

        C'est ce que nous allons faire ici, en affichant ce message dans notre navigateur.
    
    ???+success "Bonne pratique"

        Lorsqu'on utilise Eclipse, on prendre l'habitude, **à chaque modification** de code Java, d'effectuer deux actions :

        1. ++ctrl+shift+"O"++ : Permet de faire du ménage dans les imports en important ceux nécessaires, et en supprimant ceux qui ne sont pas utilisés.
            
            Les `import java.io.*` par exemple sont remplacés par l'import des classes utilisées uniquement.
            
            Sans cette action, des erreurs sont signalées ici.
        2. ++ctrl+shift+"F"++ : Permet de formater le code, de la même façon pour tout le monde.<br>C'est vraiment un plus si le code de tout le monde est formaté de la même façon. Cela permet de le comprendre beaucoup plus rapidement !


8. C'est bon, notre _Servlet_ est **créée** ! Il n'y a plus qu'à la **déployer**.

    Pour cela, on va la recenser dans le **descripteur de déploiement**. C'est le fichier 📄`web.xml` présent dans le dossier 📂`webapp\WEB-INF`.

    ``` xml title="Le fichier 📄 web.xml" linenums="1"
    <?xml version="1.0" encoding="UTF-8"?>
    <web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	         xmlns="http://xmlns.jcp.org/xml/ns/javaee"
	         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
	         id="WebApp_ID" version="4.0">
        <display-name>PremierProjet</display-name>
        <servlet><!--(1)!-->
            <servlet-name>maPremiereServlet</servlet-name>
            <servlet-class>fr.univtours.polytech.premierprojet.servlet.MaPremiereServlet</servlet-class>
        </servlet>
        
        <servlet-mapping><!--(2)!-->
            <servlet-name>maPremiereServlet</servlet-name>
            <url-pattern>/bonjour</url-pattern>
        </servlet-mapping>
    </web-app>
    ```

    1. Ici, on recense la _servlet_.<br><br>
            - `<servlet-name>` : on lui donne un nom.<br><br>
            - `<servlet-class>` : on indique la classe correspondante.
            
    2. La balise `<servlet-mapping>` permet d'indiquer avec quelle(s) URL(s) on peut accéder à la _Servlet_ :<br><br>
            - `<servlet-name>` indique de quelle _servlet_ on parle,<br><br>
            - `<url-pattern>` indique quelle(s) URL(s) y accède. Ici, l'URL [http://localhost:8080/PremierProjet/bonjour](http://localhost:8080/PremierProjet/bonjour){target=_blank}.
  
    ???+ note

        Le descripteur de déploiement peut être vu comme une sorte d'annuaire, dans lequel nous allons recenser

        1. toutes les servlets définies dans l'application (on utilise la balise `<servlet>` pour cela),
        2. quelles sont les URL qu'il faut saisir pour y accéder (on utilise la balise `<servlet-mapping>` pour cela).

        Le lien entre ces deux blocs se fait via la balise `<servlet-name>`.

9. Après avoir **redémarré le serveur tomcat**, vérifier que "Hello World" est bien affiché à cette [URL](http://localhost:8080/PremierProjet/bonjour){target=_blank}.

    ???+ success "Prise en compte des modifications"

        Pour être sûr que les modifications effectuées sont bien prises en compte, le plus simple est de redémarrer le serveur Tomcat après chaque modification.
