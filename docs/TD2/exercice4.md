---
author: Benoît Domart
title: Exercice 4 - Choix de la page
tags:
  - 0-simple
---

# Exercice 4 - Choix de la page

Dans le dossier 📂`webapp`, crée 3 pages HTML, appelées 📄`page1.html`, 📄`page2.html` et 📄`page3.html`.

Chaque page ne faisant qu'afficher son numéro. Par exemple, le code de 📄`page1.html` est :

```html
<html>
    <head>
        <title>Page 1</title>
    </head>
    <body>
        Bienvenue sur la page 1 !
    </body>
</html>
```

Créé ensuite une page HTML contenant une liste déroulante avec le nom de ces 3 pages, et un bouton <bouton>Valider</bouton>.

Le clic sur ce bouton doit rediriger vers la page correspondante :

<center>
    ![Exemples](./exemple_choix_page.gif)
</center>