---
author: Benoît Domart
title: Exercice 6 - Verbe dynamique
tags:
  - 0-simple
---

# Exercice 6 - Verbe dynamique

Créé une page HTML contenant une liste déroulante avec différents verbes HTTP (GET, POST, PUT et DELETE), et un bouton <bouton>Valider</bouton>.

???+info "Remarque"

    Ici, vous pouvez accéder directement à cette page HTML, sans passer par une Servlet pour l'afficher.

    On utilisera donc une URL du type [http://localhost:8080/PremierProjet/choixVerbe.html](http://localhost:8080/PremierProjet/choixVerbe.html) pour y accéder.

Le clic sur ce bouton doit rediriger vers la page correspondante :

<center>
    ![Exemples](./exemple_choix_verbe.gif)
</center>

La Servlet appelée lors du clic sur le bouton <bouton>Valider</bouton> doit contenir les méthodes suivantes, **qui ne doivent pas être modifiées** :

```java
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    response.getWriter().append("Verbe GET appelé");
}

@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    response.getWriter().append("Verbe POST appelé");
}

@Override
protected void doPut(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    response.getWriter().append("Verbe PUT appelé");
}

@Override
protected void doDelete(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    response.getWriter().append("Verbe DELETE appelé");
}
```