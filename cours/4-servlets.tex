\section[Servlets]{Les Servlets}

\subsection{Introduction}

\begin{frame}
	\frametitlepagenumber{Problématique}
	\begin{itemize}[<+->]
		\item On sait maintenant comment communiquer avec un serveur via le Web
			\begin{center}
				\includegraphics[width=.35\linewidth]{../docs/images/requeteHTTP}
			\end{center}
		\item On souhaite créer un site Web dynamique :
			\begin{itemize}[<+->]
				\item L'exécution d'une même requête HTTP ne va pas toujours fournir exactement la même réponse.
				\item Exemple : Airbnb -- On n'obtient pas la même liste d'appartements disponibles à Paris si on demande le 1er week-end de Mars ou le 1er week-end de Juillet.
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Solution}
	\begin{itemize}
		\item On ne peut donc pas écrire directement le site en HTML : Il nous faut donc un programme qui génère dynamiquement du code HTML.
			\begin{center}
				\includegraphics[width=.55\linewidth]{../docs/images/archi-web}
			\end{center}
		\item Plusieurs solutions techniques possibles. Nous allons parler ici d'architecture JEE.
		\item En JEE, on utilise les \textit{Servlets} pour gérer les requêtes HTTP.
		\item \textit{Servlet} est un mot valise formé à partir de \textit{server} et \textit{applet}.
	\end{itemize}
\end{frame}

\subsection{Mise en place}

\begin{frame}
	\frametitlepagenumber{Les servlets}
	\begin{itemize}
		\item Une Servlet est une classe Java qui permet :
			\begin{itemize}
				\item d'intercepter une requête HTTP,
				\item de créer dynamiquement du code HTML,
				\item de renvoyer la réponse HTTP au client.
			\end{itemize}
		\item Les Servlets utilisent l'API \textbf{Java Servlet} de JEE (package \pythonGris{javax.servlet}).
		\item L'utilisation de Servlets se fait par le biais d'un \textbf{conteneur de Servlets} côté serveur (nous allons utiliser \texttt{Apache Tomcat}, puis \texttt{JBoss WildFly}).
		\item Plusieurs Servlets constituent une \textbf{application Web Java}.
		\item Chaque Servlet de l'application doit être recensée dans le \textbf{descripteur de déploiement} (une sorte d'annuaire) : c'est le fichier \directory{web.xml}.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Exemple d'utilisation d'une servlet 1/2}
	\begin{itemize}
		\item Un client (un navigateur web) fait une requête HTTP
		\item La requête est reçue par le serveur Web et envoyée au conteneur de servlets.
		\item Le conteneur détermine quelle servlet invoquer avec les paramètres relatifs à la requête et à la réponse.
		\item La servlet, par le biais de l'objet requête, lit les paramètres, détermine l'opération à exécuter et elle l'exécute.
		\item Une fois l'exécution terminée, le conteneur envoie la réponse au serveur Web qui l'envoie au client.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Exemple d'utilisation d'une servlet 2/2}
	\putat{50}{-10}
	{
	\begin{overlayarea}{15cm}{1cm}
		\only<1>{\includegraphics[width=.65\linewidth]{../docs/images/ConteneurServlet1}}
		\only<2>{\includegraphics[width=.65\linewidth]{../docs/images/ConteneurServlet2}}
		\only<3>{\includegraphics[width=.65\linewidth]{../docs/images/ConteneurServlet3}}
		\only<4>{\includegraphics[width=.65\linewidth]{../docs/images/ConteneurServlet4}}
		\only<5>{\includegraphics[width=.65\linewidth]{../docs/images/ConteneurServlet5}}
		\only<6>{\includegraphics[width=.65\linewidth]{../docs/images/ConteneurServlet6}}
		\only<7>{\includegraphics[width=.65\linewidth]{../docs/images/ConteneurServlet7}}
	\end{overlayarea}
	}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Cycle de vie des Servlets}
	\begin{enumerate}
		\item Initialisation
		\item Gestion des requêtes (GET, POST, ...)
		\item Déchargement de la servlet
	\end{enumerate}
	~\\
	\begin{itemize}
		\item détail ici :\\
			{\scriptsize\url{https://web.maths.unsw.edu.au/~lafaye/CCM/servlets/servcycle.htm}}
		\item Le conteneur de Servlets gère tout pour nous ... tant que notre classe Servlet hérite de la classe \pythonGris{javax.servlet.http.HttpServlet} !
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Java Servlet API}
	\centering
	\includegraphics[height=6cm, clip=true]{../docs/images/ServletsAPI}	
\end{frame}

\begin{frame}
	\frametitlepagenumber{Créer sa propre Servlet}
	\begin{itemize}
		\item Hériter la classe \pythonGris{javax.servlet.http.HttpServlet}
		\item Javadoc : {\scriptsize\url{https://docs.oracle.com/javaee/7/api/javax/servlet/http/HttpServlet.html}}
		\item Redéfinir les méthodes:
			\begin{itemize}
				\item \pythonGris{init()} : pour personnaliser l'initialisation de la servlet (\textbf{optionnel)}.
				\item \pythonGris{destroy()} : pour personnaliser le déchargement de la servlet (\textbf{optionnel)}.
				\item \pythonGris{doGet()} : méthode appelée par une requête avec la méthode GET.
				\item \pythonGris{doPost()} : méthode appelée par une requête avec la méthode POST.
				\item \pythonGris{service()} : méthode appelée par une requête quelconque. Si elle n'est pas redéfinie, elle appelle la bonne méthode \pythonGris{doXXX()}.
			\end{itemize}			
	\end{itemize}	
\end{frame}

\begin{frame}
	\frametitlepagenumber{Initialisation d'une Servlet}
	\begin{itemize}
		\item Il n'y a qu'\textbf{une seule instance} de cette classe, utilisée lors de tous les appels.
		\item Une Servlet est initialisée \textbf{une seule fois}, à sa première exécution.
		\item La méthode \pythonGris{init()} dérive de la classe \pythonGris{GenericServlet}.
			\begin{itemize}
				\item Son but est d'exécuter les opérations nécessaires pour l'initialisation de la servlet.
				\item par exemple ouvrir une connexion à une base de données, ...
			\end{itemize}
		\item Signature :\\
			\pythonGris{public void init(ServletConfig config) throws ServletException}			
		\item Dans la phase d'initialisation si il y a une erreur, une \pythonGris{ServletException} est générée et envoyée au conteneur.
	\end{itemize}	
\end{frame}

\begin{frame}
	\frametitlepagenumber{Cycle de vie d'une Servlet}
	\begin{itemize}
		\item La Servlet se met en attente des requêtes HTTP (GET, POST, ...)
		\item Pour répondre à une requête il y a les méthodes:
		\begin{itemize}
			\item  \pythonGris{public void service(HttpServletRequest req, HttpServletResponse res)}\\
				~~~~\pythonGris{throws ServletException, IOException}\\
				pour toutes les requêtes.
			\item \pythonGris{public void doGet(HttpServletRequest req, HttpServletResponse res)}\\
				~~~~\pythonGris{    throws ServletException, IOException}\\
				pour les requêtes de type GET HTTP.
			\item \pythonGris{public void doPost(HttpServletRequest req, HttpServletResponse res)}\\
				~~~~\pythonGris{    throws ServletException, IOException}\\
				pour les requêtes de type POST HTTP.
		\end{itemize}							
	\end{itemize}	
\end{frame}

\begin{frame}
	\frametitlepagenumber{Manipuler la requête HTTP}
	\begin{itemize}[<+->]
		\item L'objet \pythonGris{HttpServletRequest} représente la requête du client.
		\item Par cet objet on peut accéder aux paramètres de la requête.
			\begin{itemize}[<+->]
				\item Pour obtenir la valeur d'un paramètre en particulier : \pythonGris{request.getParameter(String name)}
				\item Pour les obtenir tous : \pythonGris{request.getParameterMap()}
			\end{itemize}
		\item D'autres méthodes qui peuvent être utiles :
			\begin{itemize}[<+->]
				\item \pythonGris{getMethod()} : le verbe de la requête HTTP.
				\item \pythonGris{getServeurName()} : le nom du serveur.
				\item \pythonGris{getRemoteAddr()} : l'IP du client.
				\item \pythonGris{getServerPort()} : le port sur lequel le serveur écoute.
				\item ...
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Manipuler la réponse HTTP}
	L'objet \pythonGris{HttpServletResponse} représente la réponse du serveur.
	\begin{itemize}[<+->]
		\item On peut accéder à l'objet \pythonGris{PrintWriter}, via \pythonGris{response.getWriter()}, qui permet d'écrire le code HTML qui sera interprété par le navigateur.
		\item On peut définir le type MIME de la réponse avec \pythonGris{setContentType()}
		\item On peut envoyer une erreur comme réponse :
			\begin{itemize}[<+->]
				\item \pythonGris{response.setStatus(int sc)}
				\item \pythonGris{response.sendError(String msg)}
			\end{itemize}	
	\end{itemize}
\end{frame}

\subsection{Exemple}

\begin{frame}[containsverbatim]
	\frametitlepagenumber{Premier exemple de Servlet -- Création}
	\begin{lstlisting}
public class MaPremiereServlet extends HttpServlet {

    public void init() {
        super.init();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><head></head><body>Hello World !</body></html>");
    }

    public void destroy() {
        super.destroy();
    }
}
	\end{lstlisting}
\end{frame}

\lstset{language=Xml,alsoletter={-},morekeywords={servlet,servlet-name,servlet-class,servlet-mapping,url-pattern},basicstyle=\scriptsize\ttfamily}
\begin{frame}[fragile]
	\frametitlepagenumber{Premier exemple de Servlet -- Déploiement}
	\begin{itemize}
		\item Pour que la servlet soit opérationnelle, il faut la déployer, via le descripteur de déploiement, le fichier \directory{web.xml} :
	\end{itemize}
	\begin{lstlisting}
<servlet>
  <servlet-name>maPremiereServlet</servlet-name>
  <servlet-class>fr.univtours.polytech.premierprojet.servlet.MaPremiereServlet</servlet-class>
</servlet>

<servlet-mapping>
  <servlet-name>maPremiereServlet</servlet-name>
  <url-pattern>/bonjour</url-pattern>
</servlet-mapping>
	\end{lstlisting}
	\lstset{language=Java,basicstyle=\scriptsize\ttfamily}
	\uncover<2->{
		\begin{itemize}
			\item L'exécution de la requête {\scriptsize\url{http://localhost:8080/PremierProjet/bonjour}} va exécuter la méthode \pythonGris{service()} de la classe\\
				\pythonGris{fr.univtours.polytech.premierprojet.servlet.MaPremiereServlet}, qui va rediriger vers la méthode \pythonGris{doGet()}.
		\end{itemize}
	}
\end{frame}

\lstset{language=Xml,alsoletter={-},morekeywords={servlet,servlet-name,servlet-class,servlet-mapping,url-pattern},basicstyle=\scriptsize\ttfamily}
\begin{frame}
	\frametitlepagenumber{Le descripteur de déploiement}
	\begin{itemize}
		\item[] Lorsqu'une requête HTTP est exécutée, le conteneur de Servlets \og \textit{regarde} \fg{} dans le fichier \directory{web.xml} :
		\item Il cherche l'action à effectuer via la balise \pythonGris{<url-pattern>}.
		\item Il récupère la Servlet qui correspond via la balise \pythonGris{<servlet-name>}.
		\item Il exécute le classe correspondante via la balise \pythonGris{<servlet-class>}, plus précisément la méthode \pythonGris{service}, qui redirige vers la bonne méthode \pythonGris{doXXX}, en fonction de la méthode HTTP (GET, POST, ...).
	\end{itemize}
\end{frame}

\subsection{Redirection vers une vue}

\lstset{language=Java,basicstyle=\scriptsize\ttfamily}
\begin{frame}
	\frametitlepagenumber{Redirection vers une vue (page Web)}
	\begin{itemize}[<+->]
		\item Il n'est pas très pratique d'écrire du code HTML dans la Servlet. On va donc utiliser les Servlet pour rediriger la requête HTTP vers la bonne vue. Il y a deux façons de faire :
			\begin{itemize}[<+->]
				\item On peut effectuer une redirection de la requête avec la méthode\\
					\pythonGris{RequestDispatcher dispatcher = request.getRequestDispatcher(String location);}\\
					\pythonGris{dispatcher.forward(request, response)}\\
					La requête initiale est conservée.
				\item On peut effectuer une redirection de la requête avec la méthode\\
					\pythonGris{response.sendRedirect(String location)}\\
					La requête initiale est supprimée, une nouvelle est créée.
			\end{itemize}
		\item \pythonGris{RequestDispatcher.forward()} vs \pythonGris{HttpServletResponse.sendRedirect()} :\\
			{\tiny\url{https://stackoverflow.com/questions/2047122/requestdispatcher-forward-vs-httpservletresponse-sendredirect}}
	\end{itemize}
\end{frame}

\lstset{language=Java,basicstyle=\scriptsize\ttfamily}
\begin{frame}[containsverbatim]
	\frametitlepagenumber{Deuxième exemple, avec accès aux paramètres -- 1/3}
	\begin{itemize}[<+->]
		\item On souhaite donc maintenant que le premier accès, lorsqu'on saisit l'URL {\scriptsize\url{http://localhost:8080/PremierProjet/bonjour}}, ou qu'on clic sur le lien, nous affiche une page HTML.
		\item On va donc modifier la méthode \pythonGris{doGet} pour qu'elle redirige l'utilisateur :
	\end{itemize}
	\begin{lstlisting}
public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    RequestDispatcher dispatcher = request.getRequestDispatcher("premierFormulaire.html");
    dispatcher.forward(request, response);
}
	\end{lstlisting}
\end{frame}

\lstset{language=HTML,basicstyle=\scriptsize\ttfamily}
\begin{frame}[fragile]
	\frametitlepagenumber{Deuxième exemple, avec accès aux paramètres -- 2/3}
	On considère la page HTML suivante (\directory{premierFormulaire.html}) :
	\begin{lstlisting}
<html>
    <head>
        <title>1er formulaire</title>
    </head>
    <body>
        <form method="post" action="bonjour">
            <input type="text" name="prenom"/>
            <input type="password" name="mdp"/>
            <input type="submit" value="Valider"/>
        </form>
    </body>
</html>
	\end{lstlisting}
	{\small
		\uncover<2->{Lorsque l'utilisateur clique sur le bouton "Valider", cela soumet le formulaire, en exécutant la Servlet \pythonGris{bonjour}, définie dans le descripteur de déploiement \directory{web.xml}, et plus précisément sa méthode \pythonGris{doPost()} (car la méthode indiquée est \pythonGris{post}).}
	}
\end{frame}

\lstset{language=Java,basicstyle=\scriptsize\ttfamily}
\begin{frame}[fragile]
	\frametitlepagenumber{Deuxième exemple, avec accès aux paramètres -- 3/3}
	\begin{lstlisting}
public class MaPremiereServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        // Récupération de la valeur du champ dont l'attribut "name" est "prenom".
        String prenom = request.getParameter("prenom");
        // Génération du code HTML.
        PrintWriter out = response.getWriter();
        out.println("<html><head></head><body>Salut " + prenom + " !</body></html>");
    }
}	
	\end{lstlisting}
	~\\
	\uncover<2->{Le code généré est dynamique.}
\end{frame}

\subsection{Les servlets en résumé}

\lstset{language=Html,basicstyle=\scriptsize\ttfamily}
\begin{frame}
	\frametitlepagenumber{Les servlets en résumé}
	\begin{enumerate}[<+->]
		\item L'utilisateur exécute une requête HTTP GET, qui exécute la méthode \pythonGris{service} de la Servlet correspondante, qui (si elle n'est pas surchargée) appelle la méthode \pythonGris{doGet}, qui redirige la requête HTTP vers la bonne page HTML.
		\item Cette page HTML contient un formulaire (balise \pythonGris{<form>}), qui :
			\begin{itemize}
				\item précise la méthode avec l'attribut \pythonGris{"method"} (\pythonGris{"post"}),
				\item indique l'action à exécuter lorsque l'utilisateur soumet le formulaire (attribut \pythonGris{"action"}),
				\item contient éventuellement des paramètres (attribut \pythonGris{"name"} des balises HTML\\
					\pythonGris{<input type="xxx"/>}).
			\end{itemize}
		\item La méthode \pythonGris{service(request, response)} de cette classe est exécutée, qui (si elle n'est pas surchargée) appelle la méthode \pythonGris{doXXX(request, response)} qui correspond, et qui génère la réponse :
			\begin{itemize}
				\item Soit du contenu HTML via le \pythonGris{writer}.
				\item Soit une redirection vers une page HTML (ou éventuellement une autre servlet).
			\end{itemize}
	\end{enumerate}
\end{frame}
